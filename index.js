const express = require('express')
const app = express()

app.get('/', (req, res) => {
    res.sendFile(__dirname+"/home_page.html")
})

app.get('/photo', (req, res) => {
    res.sendFile(__dirname+"/profile_pic.jpeg")
})

app.listen(8080, () => {
    console.log('Server is up on 8080')
})
